const { GraphQLServer } = require('graphql-yoga')
const fetch = require('node-fetch')

const BASE_URL = 'https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development'

const applicationId = 'steve.urkel'


const typeDefs = `
    type Query {
        getIssue(id: String!): Issue
        getAllIssues: [Issue!]!
    }
    type Mutation {
        setIssue(input: IssueInput): Issue
    }
    type Issue {
        id: String
        title: String
        text: String
        severity: String
        reporter: String
    }
    input IssueInput {
        title: String
        text: String
        severity: String
        reporter: String
    }
`

const resolvers = {
    Query: {
        getIssue: (_, { id }) => fetch(`${BASE_URL}/pairs/${id}`, {
            method: "GET",
            headers: { 
                "Content-Type": "application/json",
                "x-application-id": `${applicationId}` }
            }).then(res => res.json().value).then(x => (console.log(x) || x)),
        getAllIssues: (_, ) => fetch(`${BASE_URL}/pairs/`, {
            method: "GET",
            headers: { 
                "Content-Type": "application/json",
                "x-application-id": `${applicationId}` }
            }).then(res => res.json()).then(json => Array.from(json).map(pair => JSON.parse(pair.value).input))
    },
    Mutation: {
        setIssue: (_, { input }) => {
            id = require('crypto').randomBytes(16).toString('hex');
            fetch(`${BASE_URL}/pairs/${id}`, {
                method: "PUT",
                headers: { 
                    "Content-Type": "application/json",
                    "x-application-id": `${applicationId}` },
                body: JSON.stringify({input})
            }).then(res => res.json())
        }
    }
}

const server = new GraphQLServer({ typeDefs, resolvers })

server.start({ 
    playground: '/playground',
    //port: process.env.PORT 
    port: 8082
})