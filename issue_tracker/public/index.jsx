const { React, ReactDOM, Component } = window;
const {
  BrowserRouter: Router,
  NavLink,
  Switch,
  Route,
  withRouter
} = window.ReactRouterDOM;

const GRAPHQL_URL = "http://localhost:8082"

/**
 * Componente Home
 */
class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="container">
        <h1 style={{ textAlign: "center" }}>Home</h1>
      </div>
    );
  }
}

/**
 * Componente que muestra la lista de issues que hay
 */
class IssueList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      issues: []
    };
    this.loadIssues = this.loadIssues.bind(this);
  }

  //Carga los issues del stub
  loadIssues() {
    fetch(GRAPHQL_URL, {
      method: "POST",
      headers: {"Content-type":"application/json"},
      body: JSON.stringify({
        query:`query{
          getAllIssues{
            id
            title,
            text,
            severity,
            reporter
          }
        }`
      })
    })
    .then(resp => {
      return resp.json();
    })
    .then(json => {
      if (json.errors) {
        alert("Ha ocurrido un error. Vuelve a intentarlo.");
      } else {
        this.setState({ issues: json.data.getAllIssues });
      }
    });
  }

  //Carga el componente Issue con los datos del tr clicked
  loadIssue(event) {
    const title = event.currentTarget.dataset.id;
    const objectEvent = this.state.issues.filter(issue => {
      return issue.title == title;
    })[0];

    this.props.history.push({
      pathname: "/issue",
      state: { issueInfo: objectEvent }
    });
  }

  componentDidMount() {
    this.loadIssues();
  }

  render() {
    const { issues } = this.state;

    return (
      <div className="container">
        <h1>Lista de Issues</h1>
        <table>
          <thead>
            <tr>
              <th>Title</th>
              <th>Created</th>
              <th>Reporter</th>
              <th>Status</th>
              <th>Severity</th>
            </tr>
          </thead>
          <tbody>
            {/* Creacion de la tabla con los issues 
            a partir de los datos obtenidos del stub*/}
            {issues.map(item => (
              <tr
                data-id={item.title}
                onClick={this.loadIssue.bind(this)}
                className="clickableRow"
              >
                {Object.keys(item).map(
                  (key, idx) => idx != 1 && <td>{item[key]}</td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

/**
 * Componente para la visualizacion de un issue concreto
 */
class Issue extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    //Valores del issue obtenidos de ListIssue
    const issueInfo = this.props.location.state.issueInfo;

    return (
      <div className="container">
        <h1>Issue info</h1>
        <fieldset>
          <legend>{issueInfo.title}</legend>
          <form>
            <div style={{ marginBottom: "10px" }}>
              <span>
                <b>
                  {issueInfo.reporter} ({issueInfo.dateCreated})
                </b>
              </span>
            </div>
            <span>{issueInfo.text}</span>
          </form>
        </fieldset>
      </div>
    );
  }
}

/**
 * Componente para la creacion de un nuevo issue
 */
class FormIssue extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      reporter: "",
      severity: "high",
      issueText: ""
    };
  }

  //Funcion para el onChange de los inputs
  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  //Funcion para comprobar que no esten sin rellenar ningun input
  canBeSubmitted() {
    const { title, reporter, severity, issueText } = this.state;
    return (
      title.length > 0 &&
      reporter.length > 0 &&
      severity.length > 0 &&
      issueText.length > 0
    );
  }

  //Funcion onSubmit issue
  submitForm(event) {
    event.preventDefault();
    fetch(GRAPHQL_URL, {
      method: "POST",
      headers: {"Content-type":"application/json"},
      body: JSON.stringify({
        query:`mutation{
          setIssue( input: {
            title: "${this.state.title}",
            text: "${this.state.issueText}",
            severity: "${this.state.severity}",
            reporter: "${this.state.reporter}",
          }
          ){
            title
          }
        }`
      })
    })
    .then(resp => {
      return resp.json();
    });
    this.props.history.push("/issues");
  }

  render() {
    const submitReady = this.canBeSubmitted();
    return (
      <div className="container">
        <h1>Create Issue</h1>
        <form onSubmit={this.submitForm.bind(this)}>
          <label htmlFor="title">Titulo</label>
          <input
            type="text"
            id="title"
            name="title"
            value={this.state.title}
            onChange={this.handleChange}
            placeholder="Titulo del problema"
          ></input>

          <label htmlFor="reporter">Nombre</label>
          <input
            type="text"
            id="reporter"
            name="reporter"
            value={this.state.reporter}
            onChange={this.handleChange}
            placeholder="Nombre del reportador"
          ></input>

          <label htmlFor="severity">Severity</label>
          <select
            id="severity"
            name="severity"
            value={this.state.severity}
            onChange={this.handleChange}
          >
            <option value="High">High</option>
            <option value="Medium">Medium</option>
            <option value="Low">Low</option>
          </select>

          <label htmlFor="issueText">Descripción del problema</label>
          <textarea
            id="issueText"
            name="issueText"
            value={this.state.issueText}
            onChange={this.handleChange}
            placeholder="Describe el problema..."
            style={{ height: "150px" }}
          ></textarea>

          <input disabled={!submitReady} type="submit" value="Submit"></input>
        </form>
      </div>
    );
  }
}

/**
 * Main component, render nav y router
 */
class SPA extends React.Component {
  render() {
    return (
      <Router>
        <nav>
          <ul>
            <li>
              <NavLink activeClassName="active-nav" exact to="/">
                Home
              </NavLink>
            </li>
            <li>
              <NavLink exact activeClassName="active-nav" to="/create_issue">
                Create Issue
              </NavLink>
            </li>
            <li>
              <NavLink exact activeClassName="active-nav" to="/issues">
                List Issues
              </NavLink>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/issues" component={IssueList} />
          <Route path="/issue" component={Issue} />
          <Route path="/create_issue" component={FormIssue} />
          <Route path="/" component={Home} />
        </Switch>
      </Router>
    );
  }
}

ReactDOM.render(<SPA />, document.getElementById("root"));
