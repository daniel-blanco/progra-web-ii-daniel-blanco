const { React, ReactDOM } = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;

const Title = React.createElement('h1', {}, 'Agregador de noticias')

const Topbar = React.createElement('div', {className: 'topbar'}, Title);

const Noticia1 = () => {
  return (
      <div>
        <h1>Noticia 1</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non pulvinar purus, et efficitur dui. Fusce dapibus nisi sed mauris condimentum consectetur. Quisque rhoncus bibendum lacus, et maximus nisi tempus vulputate. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus pretium tempus mollis. Praesent vulputate tortor sed risus hendrerit tempus. Aliquam luctus varius ultricies. Ut id aliquam nulla, eget auctor risus. Pellentesque porttitor porta lacus, fringilla porttitor eros congue id. Vestibulum sed ullamcorper sem, sed aliquet ligula. In metus velit, blandit non rutrum eget, vulputate non libero.</p>
      </div>
  );
};

const Noticia2 = () => {
  return (
      <div>
        <h1>Noticia 2</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer luctus at dui at fermentum. Quisque ut suscipit leo, sed lobortis tellus. Nam mollis erat nec maximus semper. Morbi vitae eleifend nisi. Sed a arcu suscipit nulla volutpat iaculis nec vitae tellus. Etiam placerat et dui sed placerat. Curabitur vestibulum quam et ullamcorper faucibus. Suspendisse ut egestas nisl, quis tempor lectus. Proin dapibus, justo at viverra aliquet, diam nulla cursus augue, et vulputate tortor nisi sit amet odio. Nullam et enim non tellus ullamcorper semper. Fusce sit amet diam magna. Phasellus libero libero, gravida ut est ut, lacinia iaculis nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc consectetur arcu ex, ac volutpat mauris commodo vitae.</p>
      </div>
  );
};

const Noticia3 = () => {
  return (
      <div>
        <h1>Noticia 3</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus neque quis dolor egestas, at varius sapien ornare. Nullam mi dui, pharetra nec mauris id, bibendum pretium lectus. Aliquam elementum mauris id mi placerat, sit amet laoreet ante pulvinar. Pellentesque vulputate tortor ut venenatis sodales. Suspendisse scelerisque mi tellus. In vestibulum orci id lacus ullamcorper consectetur. Cras nec justo non magna cursus congue et nec nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus vel maximus dolor. Fusce eu eleifend metus, sit amet consectetur mi. Etiam vehicula, tortor ut condimentum aliquam, diam orci dignissim enim, vitae euismod dolor est a sapien. Donec consectetur nisi sit amet consequat malesuada. Fusce in sem at nunc cursus lobortis. Donec et mattis tortor. Fusce efficitur lorem at posuere molestie. Proin vel lectus et lorem fringilla maximus.</p>
      </div>
  );
};

const Nav = () => {
  return (
    <Router>
      <nav>
        <div>
          <Link to="/n1">Noticia 1</Link>
        </div>
        <div>
          <Link to="/n2">Noticia 2</Link>
        </div>
        <div>
          <Link to="/n3">Noticia 3</Link>
        </div>
      </nav>
      <Switch>
        <Route path="/n1">
          <Noticia1 />
        </Route>
        <Route path="/n2">
          <Noticia2 />
        </Route>
        <Route path="/n3">
          <Noticia3 />
        </Route>
      </Switch>
    </Router>
  );
};

const SPA = React.createElement('div', {}, [Topbar, Nav()]);

ReactDOM.render(
  SPA,
  document.getElementById('root')
);
